# Useful info
Git
-
* [conventional commits](readme/COMMIT_CONVENTION.md)
* [alias](readme/GIT_ALIAS.md)
* [commands](readme/GIT_COMMANDS.md)
