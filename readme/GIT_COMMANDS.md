# Useful commands

| Command | Description |
| --- | --- |
| `git reset --hard HEAD~1` | Remove last commit and move to the previous one |
| `git commit --amend` | Rename last commit |
