import * as actionType from "../constants"

const initialState = {
  todoList: [],
  error: null,
  loading: null,
}

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case actionType.FETCH_TODO_LIST_START:
      return {
        ...state,
        error: null,
        loading: true,
      }
    case actionType.FETCH_TODO_LIST_SUCCESS:
      return {
        ...state,
        todoList: action.todoList,
        error: null,
        loading: false,
      }
    case actionType.FETCH_TODO_LIST_FAIL:
      return {
        ...state,
        error: action.error,
        loading: false,
      }
    case actionType.ADD_TODO_SUCCESS:
      return {
        ...state,
        todoList: state.todoList.concat(action.newTodo),
      }
    case actionType.ADD_TODO_FAIL:
      return {
        ...state,
        error: action.error,
      }
    case actionType.CHANGE_TODO_SUCCESS:
      const updatedList = state.todoList.map(item => {
        if (action.updatedTodo.id === item.id) {
          return action.updatedTodo
        }
        return item
      })
      return {
        ...state,
        todoList: updatedList,
      }
    case actionType.CHANGE_TODO_FAIL:
      return {
        ...state,
        error: action.error,
      }
    case actionType.DELETE_TODO_SUCCESS:
      return {
        ...state,
        todoList: state.todoList.filter(item => item.id !== action.todoId),
      }
    case actionType.DELETE_TODO_FAIL:
      return {
        ...state,
        error: action.error,
      }
    default:
      return state
  }
}

export default reducer
