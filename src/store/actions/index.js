import * as actionType from "../constants"

const fetchTodoList = completed => ({
  type: actionType.FETCH_TODO_LIST,
  completed,
})

const addTodo = newTodo => ({
  type: actionType.ADD_TODO,
  newTodo,
})

const changeTodo = updatedTodo => ({
  type: actionType.CHANGE_TODO,
  updatedTodo,
})

const deleteTodo = todoId => ({
  type: actionType.DELETE_TODO,
  todoId,
})

export const todoActions = {
  fetchTodoList,
  addTodo,
  deleteTodo,
  changeTodo,
}
