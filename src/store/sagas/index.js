import { takeEvery, all, call, put } from "redux-saga/effects"
import axios from "axios"

import * as actionTypes from "~/store/constants"

export function* fetchTodoList({ completed }) {
  try {
    yield put({ type: actionTypes.FETCH_TODO_LIST_START })
    const { data: todoList } = yield call(
      axios.get,
      `http://localhost:3001/list${
        completed === undefined ? "/" : `?completed=${completed}`
      }`,
    )
    yield put({ type: actionTypes.FETCH_TODO_LIST_SUCCESS, todoList })
  } catch (error) {
    yield put({ type: actionTypes.FETCH_TODO_LIST_FAIL, error })
  }
}

export function* addTodo({ newTodo }) {
  try {
    yield call(axios.post, "http://localhost:3001/list/", newTodo)
    yield put({ type: actionTypes.ADD_TODO_SUCCESS, newTodo })
  } catch (error) {
    yield put({ type: actionTypes.ADD_TODO_FAIL, error })
  }
}

export function* changeTodo({ updatedTodo }) {
  try {
    yield call(
      axios.put,
      `http://localhost:3001/list/${updatedTodo.id}`,
      updatedTodo,
    )
    yield put({ type: actionTypes.CHANGE_TODO_SUCCESS, updatedTodo })
  } catch (error) {
    yield put({ type: actionTypes.CHANGE_TODO_FAIL, error })
  }
}

export function* deleteTodo({ todoId }) {
  try {
    yield call(axios.delete, `http://localhost:3001/list/${todoId}`)
    yield put({ type: actionTypes.DELETE_TODO_SUCCESS, todoId })
  } catch (error) {
    yield put({ type: actionTypes.DELETE_TODO_FAIL, error })
  }
}

function* watchTodo() {
  yield all([
    takeEvery(actionTypes.FETCH_TODO_LIST, fetchTodoList),
    takeEvery(actionTypes.ADD_TODO, addTodo),
    takeEvery(actionTypes.CHANGE_TODO, changeTodo),
    takeEvery(actionTypes.DELETE_TODO, deleteTodo),
  ])
}

export default watchTodo
