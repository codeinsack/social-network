import React from "react"
import styled from "styled-components"

import { TextField, Button, Tabs, Tab } from "@material-ui/core"

const Panel = ({
  addNewTodo,
  enterTodoName,
  value,
  filterByCompleteness,
  tabIndex,
}) => (
  <>
    <AddTodoWrapper>
      <TextField
        value={value}
        variant="outlined"
        placeholder="Enter name"
        margin="dense"
        onChange={enterTodoName}
      />
      <Button variant="outlined" color="primary" onClick={addNewTodo}>
        Add
      </Button>
    </AddTodoWrapper>
    <TabsWrapper>
      <Tabs value={tabIndex} onChange={filterByCompleteness}>
        <Tab label="All" />
        <Tab label="Completed" />
        <Tab label="Not completed" />
      </Tabs>
    </TabsWrapper>
  </>
)

export default Panel

const AddTodoWrapper = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  margin: 20px 0;
  .MuiTextField-root {
    margin: 0;
    width: 100%;
  }
  .MuiInputBase-root {
    font-size: 14px;
  }
  .MuiButton-root {
    margin-left: 10px;
  }
`

const TabsWrapper = styled.div`
  display: flex;
  justify-content: center;
`
