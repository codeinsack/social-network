import React from "react"
import styled from "styled-components"

import IconButton from "@material-ui/core/IconButton"
import CheckCircleIcon from "@material-ui/icons/CheckCircleOutline"
import DeleteIcon from "@material-ui/icons/DeleteOutline"

import Item from "./Item"

const List = ({ todoList, changeCompleteness, deleteItem }) => (
  <OrderedList>
    {todoList.map(todo => (
      <ItemWrapper key={todo.id}>
        <Item todo={todo} />
        <IconButton
          size="small"
          disableRipple
          onClick={changeCompleteness(todo.id)}
        >
          <CheckCircleIcon />
        </IconButton>
        <IconButton size="small" disableRipple onClick={deleteItem(todo.id)}>
          <DeleteIcon />
        </IconButton>
      </ItemWrapper>
    ))}
  </OrderedList>
)

export default List

const OrderedList = styled.ol`
  padding-left: 0;
`

const ItemWrapper = styled.li`
  display: flex;
  .MuiIconButton-root {
    width: 45px;
    &:hover {
      background-color: transparent;
    }
  }
`
