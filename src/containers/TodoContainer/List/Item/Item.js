import React from "react"
import styled from "styled-components"

import {
  ExpansionPanel,
  ExpansionPanelDetails,
  ExpansionPanelSummary,
  Typography,
} from "@material-ui/core"
import ExpandMoreIcon from "@material-ui/icons/ExpandMore"

const Item = ({ todo }) => (
  <ExpansionPanelStyled completed={todo.completed.toString()}>
    <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
      <Typography>{todo.name}</Typography>
    </ExpansionPanelSummary>
    <ExpansionPanelDetails>
      <Typography>
        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse
        malesuada lacus ex, sit amet blandit leo lobortis eget.
      </Typography>
    </ExpansionPanelDetails>
  </ExpansionPanelStyled>
)

export default Item

const ExpansionPanelStyled = styled(ExpansionPanel)`
  p {
    text-decoration: ${props =>
      props.completed === "true" ? "line-through" : "none"};
  }
`
