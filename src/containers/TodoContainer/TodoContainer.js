import React, { useEffect, useState } from "react"
import { bindActionCreators } from "redux"
import { connect } from "react-redux"

import Container from "@material-ui/core/Container"

import { todoActions } from "~/store/actions"

import List from "./List"
import Panel from "./Panel"

const TodoContainer = ({ actions, todoList }) => {
  const [todo, setTodo] = useState("")
  const [tabIndex, setTabIndex] = useState(0)

  useEffect(() => {
    actions.fetchTodoList()
  }, [actions])

  const onTabChange = (event, index) => {
    if (index === 1) {
      actions.fetchTodoList(true)
    } else if (index === 2) {
      actions.fetchTodoList(false)
    } else {
      actions.fetchTodoList()
    }
    setTabIndex(index)
  }

  const onAddButtonClick = async () => {
    if (!todo) return
    const newTodo = {
      id: Math.random(),
      name: todo,
      completed: false,
    }
    setTodo("")
    actions.addTodo(newTodo)
  }

  const onTodoInputChange = ({ target: { value } }) => {
    setTodo(value)
  }

  const onCompletenessButtonClick = id => () => {
    const targetTodo = todoList.find(item => item.id === id)
    actions.changeTodo({
      ...targetTodo,
      completed: !targetTodo.completed,
    })
  }

  const onDeleteButtonClick = id => () => {
    actions.deleteTodo(id)
  }

  return (
    <Container maxWidth="sm">
      <Panel
        addNewTodo={onAddButtonClick}
        enterTodoName={onTodoInputChange}
        filterByCompleteness={onTabChange}
        tabIndex={tabIndex}
        value={todo}
      />
      <List
        todoList={todoList}
        changeCompleteness={onCompletenessButtonClick}
        deleteItem={onDeleteButtonClick}
      />
    </Container>
  )
}

export default connect(
  state => ({
    todoList: state.todoList,
  }),
  dispatch => ({
    actions: bindActionCreators(
      {
        ...todoActions,
      },
      dispatch,
    ),
  }),
)(TodoContainer)
