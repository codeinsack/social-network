import React from "react"

import { Typography } from "@material-ui/core"

import TodoContainer from "~/containers/TodoContainer"

const App = () => {
  return (
    <>
      <Typography align="center" variant="h5">
        Hello from Social Network!
      </Typography>
      <TodoContainer />
    </>
  )
}

export default App
